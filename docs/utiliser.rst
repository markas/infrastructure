.. _utiliser:

Guide d'utilisation
===================

.. _compte:

Création d'un compte utilisateur
--------------------------------

Nous fournissons des services permettant de partager des fichiers et
gérer des agendas en ligne. Ces services sont basés sur le logiciel
libre `Nextcloud <https://nextcloud.com/>`_.

Pour se créer un compte, il nécessaire d'être connu ou de se faire connaître de l'association (:ref:`voir la page de contact <contact>`) :

1. Rendez-vous https://cloud.markas.fr
2. Cliquer sur `S'enregistrer`
3. Saisissez une addresse courriel valide puis cliquez sur `Demander un lien de vérification`
4. Vous recevrez un courriel de registration@markas.fr
5. Cliquez sur le lien de vérification
6. Votre compte doit désormais être activé par un administrateur qui
   confirmera la création de votre compte

Une fois votre compte activé, vous pourrez vous authentifier sur
https://cloud.markas.fr.

Vous disposerez ensuite d'un mois pour régulariser votre adhésion à l'association, dont les modalités vous seront précisées par un mail de bienvenue :

- prise de connaissance des :ref:`statuts <statuts>`, du :ref:`règlement intérieur <reglement_interieur>` et des :ref:`conditions générales d'utilisation <cgu>`,
- paiement de la cotisation,
- communication de vos informations de contact (nom, prénom et adresse).

Services disponibles
--------------------

Notre instance Nextcloud permet de

- stocker des fichiers (40 Go par défaut)
- partager des fichiers
- gérer des agendas en lignes
- synchroniser des contacts
- éditer des fichiers texte, de type office

Conditions générales d'utilisation (CGU)
----------------------------------------

.. toctree::
   :maxdepth: 2

   cgu.rst
