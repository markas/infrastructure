.. _cgu:

Préambule
=========

En utilisant ce service, vous acceptez d’être lié·e par les conditions ci-dessous.

Markas se réserve le droit de mettre à jour et modifier ces conditions ; elle en
informera alors ses utilisateurs.

Conditions du service
=====================

1. L’utilisation du service se fait à vos propres risques. Le service
   est fourni tel quel.
2. L’adhésion à l’assocation Markas est requise pour l’utilisation du
   service.
3. Vous êtes responsable de la sécurité de votre compte et de votre mot
   de passe. Markas ne peut pas et ne sera pas responsable de toutes
   pertes ou dommages résultant de votre non-respect de cette obligation
   de sécurité.
4. Vous êtes responsable de tout contenu affiché et de l’activité qui se
   produit sous votre compte.
5. Vous ne pouvez pas utiliser le service à des fins illégales ou non
   autorisées. Vous ne devez pas transgresser les lois de votre pays et de la
   France.
6. Vous ne pouvez pas vendre, échanger, revendre, ou exploiter dans un
   but commercial non autorisé un compte du service utilisé.
7. Les comptes ne peuvent être créés et utilisés que par des humain·es.
   Les comptes créés par les robots ou autres méthodes automatisées
   pourront être supprimés sans avertissement.
8. Vous ne devez pas modifier un autre site afin de signifier faussement
   qu’il est associé avec ce service Markas.
9. Vous ne devez pas nuire (utilisation abusive) au bon fonctionnement
   du service.

La violation de l’un de ces accords entraînera la résiliation de votre compte.

Vous comprenez et acceptez que l’association Markas ne puisse être tenue
responsable pour les contenus publiés sur ce service.

1. Markas ne garantit pas que :

   -  le service répondra à vos besoins spécifiques,
   -  le service sera ininterrompu ou exempte de bugs,
   -  que les erreurs dans le service seront corrigées.

2. Vous comprenez et acceptez que Markas ne puisse être tenue
   responsable de tous dommages directs, indirects, ou fortuits,
   comprenant les dommages pour perte de profits, de clientèle, d’accès,
   de données ou d’autres pertes intangibles (même si Markas est informé
   de la possibilité de tels dommages) et qui résulteraient de :

   - l’utilisation ou de l’impossibilité d’utiliser le service,
   - l’accès non autorisé ou altéré de la transmission des données,
   - les déclarations ou les agissements d’un tiers sur le service ,
   - la résiliation de votre compte ,
   - toute autre question relative au service.

3. L’échec de Markas à exercer ou à appliquer tout droit ou disposition
   des conditions générales d’utilisation ne constitue pas une
   renonciation à ce droit ou à cette disposition. Les conditions
   d’utilisation constituent l’intégralité de l’accord entre vous et
   Markas et régissent votre utilisation du service, remplaçant tous les
   accords antérieurs entre vous et Markas (y compris les versions
   précédentes des conditions générales d’utilisation).

Modifications du service
========================

1. Markas se réserve le droit, à tout moment de modifier ou
   d’interrompre, temporairement ou définitivement, le service avec ou
   sans préavis.
2. Markas ne sera pas responsable envers vous ou tout tiers pour toute
   modification, suspension ou interruption du service.

Droit d’auteur sur le contenu
=============================

1. Vous ne pouvez pas envoyer, télécharger, distribuer, diffuser tout
   contenu illégal, diffamatoire, harcelant, abusif, frauduleux,
   contrefait, obscène ou autrement répréhensible.
2. Nous ne revendiquons aucun droit sur vos données : textes, images,
   son, vidéo, ou tout autre élément, que vous téléchargez ou
   transmettez depuis votre compte.
3. Nous n’utiliserons pas votre contenu pour un autre usage que de vous
   fournir le service.
4. Vous ne devez pas télécharger ou rendre disponible tout contenu qui
   porte atteinte aux droits de quelqu’un d’autre.
5. Nous nous réservons le droit de supprimer tout contenu nous
   paraissant non pertinent pour l’usage du service, selon notre seul
   jugement.
6. Nous pouvons, si nécessaire, supprimer ou empêcher la diffusion de
   tout contenu sur le service qui ne respecterait pas les présentes
   conditions.

Édition et partage de données
=============================

1. Les fichiers que vous créez avec le service peuvent être – si vous le
   souhaitez – lus, copiés, utilisés et redistribués par des gens que
   vous connaissez ou non.
2. En rendant publiques vos données, vous reconnaissez et acceptez que
   toute personne puisse les consulter sans restrictions.
3. Le service peut également vous proposer la possibilité d’autoriser
   l’accès et le travail collaboratif sur ses documents de manière
   restreinte à un·e ou plusieurs autres utilisateur·ices.
4. Markas ne peut être tenue responsable de tout problème résultant du
   partage ou de la publication de données entre utilisateur·ices.

Résiliation
===========

1. Markas a le droit de suspendre ou de résilier votre compte et de
   refuser toute utilisation actuelle ou future du service. Cette
   résiliation du service entraînera la désactivation de l’accès à votre
   compte.
2. Markas se réserve le droit de refuser le service à n’importe qui pour
   n’importe quelle raison à tout moment.
3. La perte du statut de membre entraîne la désactivation de l’accès à
   votre compte. En l’absence de régularisation dans les 30 jours, le
   compte et les données associées seront supprimées définitivement.

Données personnelles
====================

Conformément à l’article 34 de la loi « Informatique et Libertés »,
Markas garantit à l’utilisateur·ice un droit d’opposition, d’accès et de
rectification sur les données nominatives le·la concernant.

L’utilisateur·ice a la possibilité d’exercer ce droit en :ref:`en nous
contactant <contact>`.

1. L’utilisation du service requière la création d’un compte, pour
   lequel certaines informations personnelles sont demandées :

   - une adresse e-mail valide,
   - vos nom et prénom,
   - votre adresse.

2. Tout comme d’autres services en ligne, Markas enregistre
   automatiquement certaines informations concernant votre utilisation
   du service telles que l’activité du compte (exemple : espace de
   stockage occupé, nombre d’entrées, mesures prises), les données
   affichées ou cliquées (exemple : liens, éléments de l’interface
   utilisateur), et d’autres informations pour vous identifier
   (exemple : type de navigateur, adresse IP, date et heure de l’accès,
   URL de référence).

3. Ces données ne sont ni vendues, ni transmises à des tiers.


.. rubric:: Notes

Version du 13/01/2022

Inspiré des `CGU de Framasoft <https://framasoft.org/fr/cgu/>`__
