L'association Markas
====================

Statuts
-------

.. toctree::
   :maxdepth: 2

   statuts.rst

Règlement intérieur
-------------------

.. toctree::
   :maxdepth: 2

   reglement_interieur.rst

.. _assoc-cotisation:

Modèle économique
-----------------

L'objectif est d'au moins couvrir les frais de fonctionnement de
l'infrastructure grâce aux cotisations des adhérents. Les adhérents
cotisent en fonction de leurs moyens.

Cotisation
----------

Comme indiqué dans :ref:`le règlement intérieur <RI-cotisation>`, le montant
minimal de la cotisation est de 1 €/an. Le prix conseillé est
de 20 €/an (au prorata des mois restants jusqu'à la fin de l'année pour la
première adhésion). Il est basé sur les frais annuels de fonctionnement,
principalement location du serveur et achat du nom de domaine, de 340 €
actuellement, et sur un nombre attendu d'adhérents d'environ 17.

Elle est exigible dans les 2 premiers mois de l'année en cours (c-à-d avant
mars), sauf pour l'adhésion initiale. Le paiement s'effectue de préférence par
virement bancaire. Un relevé d'identité bancaire du compte de l'association est
adressé par mail lors de l'accueil d'un nouvel adhérent ou de l'appel annuel à
cotisation. Afin de faciliter la gestion des adhésions, il est demandé d'indiquer
le nom et le prénom de l'adhérent dans le libellé du virement.
