Description de l'infrastructure
-------------------------------

L'infrastructure est actuellement composé d'un unique serveur.

Environement
************

Hébergement
~~~~~~~~~~~

Le serveur est un ancien serveur Kimsufi (16GB RAM, CPU Intel(R) Xeon(R)
W3520, HDD 2x2 To) hébergé par Kimsufi à Paris dans le datacenter RBX2 (Rack
23C02), loué au nom de l'association Markas.

Nom de domaine
~~~~~~~~~~~~~~

Le nom de domaine `markas.fr` est géré par Gandi au nom l'association Markas.

Système d'exploitation
~~~~~~~~~~~~~~~~~~~~~~

Nous utilisons la distribution NixOS en version |nixpkgs-branch|. La révision
NixOS actuellement déployée est |nixpkgs-url|_. Nous configurons ensuite cette
distribution pour l'adapter à nos besoins. La révision de la configuration
Markas actuellement déployée est |repository-revision|_.

Version des logiciels
~~~~~~~~~~~~~~~~~~~~~

* nextcloud: |nextcloud-version|
* nextcloud-registration: |nextcloud-registration-version|
* nextcloud-calendar: |nextcloud-calendar-version|
* nextcloud-contacts: |nextcloud-contacts-version|
* nextcloud-notes: |nextcloud-notes-version|
* nextcloud-richdocuments: |nextcloud-richdocuments-version|

Liste des membres ayant des droits d'administreurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO: lister les droits root, ssh admin nextcloud,...

Politique de sécurité
~~~~~~~~~~~~~~~~~~~~~

TODO: expliquer que l'on fait des mises à jour régulièrement, qu'on
limite les accès admin...

Contributeurs
~~~~~~~~~~~~~

https://framagit.org/markas/infrastructure/-/graphs/master

Licence
~~~~~~~

Le dépôt https://framagit.org/markas/infrastructure est sous license
`AGPLv3
<https://fr.wikipedia.org/wiki/GNU_Affero_General_Public_License>`_

Les services d'infrastructures
******************************

Afin d'assurer le bon fonctionnement des services utilisateurs, des
services d'infrastructures sont nécessaires.

Sauvegarde
~~~~~~~~~~

Une sauvegarde chiffrée est envoyée tous les jours à 4h du matin sur
le serveur `kordia.abesis.fr`. Ces sauvegardes sont réalisées par le
logiciel `Borg <https://borgbackup.readthedocs.io/en/stable/>`_.

Un deuxième serveur destiné à recevoir les sauvegardes ainsi qu'à faire le
monitoring du serveur principal sera bientôt mis en place, et remplacera
`kordia.abesis.fr`.

Monitoring et alerting
~~~~~~~~~~~~~~~~~~~~~~

Nous utilisons Prometheus et Grafana. Ces services sont consultables publiquement sur:

* https://grafana.markas.fr
* https://metriques.markas.fr

Logs
~~~~

Les logs du serveur sont exposés par `systemd-journal-gatewayd
<https://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.service.html>`_.
Ces logs ne sont accessible que par le VPN et ne sont actuellement ni
exportés, ni sauvegardés.

VPN
~~~

Un VPN est utilisé pour permettre aux membres d'accéder à des données
privées, telles que les logs. Le VPN utilisé est `Wireguard
<https://www.wireguard.com/>`_.

Mode de déploiement
~~~~~~~~~~~~~~~~~~~

La configuration du serveur est décrite dans le dépôt
|repository-url|_. Toutes les minutes, un service systemd récupère le
contenu de ce dépôt sur le serveur et applique les potentiels
changements.

Avant d'appliquer un changement, le serveur vérifie que le commit HEAD
de la branche master a bien été signé par une personne autorisée.
