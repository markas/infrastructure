{ pkgs, sources, markas-configuration }:

let

  markasRev = pkgs.lib.commitIdFromGitRepo ./../.git;

  infra = pkgs.writeTextFile {
    name = "infra.json";
    text = builtins.toJSON {
      repository = {
        url = "https://framagit.org/markas/infrastructure";
        rev = markasRev;
        rev_url = "https://framagit.org/markas/infrastructure/-/tree/${markasRev}";
      };
      nextcloud = {
        version = markas-configuration.services.nextcloud.package.version;
      };
    };
  };

  sources = pkgs.writeTextFile {
    name = "sources.json";
    text = builtins.readFile ../nix/sources.json;
  };

in pkgs.stdenv.mkDerivation {
  name = "markas-documentation";
  src = pkgs.lib.sourceByRegex ./. ["conf.py" "Makefile" ".*rst$" ".*md$"];
  buildInputs = [(
    pkgs.python3.withPackages(p: [
      p.sphinx
      p.recommonmark
      p.sphinx_rtd_theme
    ])
  )];
  buildPhase = ''
    cp ${infra} infra.json
    cp ${sources} sources.json
    mkdir -p _static/css/
    cp ${./_static/css/custom.css} _static/css/custom.css
    # Workaround for https://github.com/sphinx-doc/sphinx/issues/3451
    export SOURCE_DATE_EPOCH=$(${pkgs.coreutils}/bin/date +%s)
    SPHINXOPTS=-W make html
  '';
  installPhase = ''
    cp -r build/html $out
  '';
}
