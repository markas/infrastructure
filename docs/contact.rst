.. _contact:

Nous contacter
--------------

Par messagerie instantanée
==========================

- IRC : canal #markas sur irc.geeknode.org ou avec un navigateur en
  cliquant ici : `chat de Markas
  <https://kiwiirc.com/client/irc.geeknode.org/markas/>`_.
- Matrix : salon #markas sur le serveur `matrix.org <https://matrix.org/>`_.
  `Plusieurs clients utilisables <https://matrix.org/clients-matrix/>`_, dont
  `Element <https://fr.wikipedia.org/wiki/Element_(logiciel)>`_ (application ou
  version web).

Note : n'hésitez pas à poser vos questions. Les gens ne sont pas forcément
derrière un écran au moment où vous les posez donc il est donc possible que vous
attendiez un moment avant qu'une personne réagisse…

Par courriel
============

- `contact CHEZ markas POINT fr <mailto:contact CHEZ markas POINT fr>`_
- liste de diffusion : `markas CHEZ framalistes POINT org <mailto:markas CHEZ
  framalistes POINT org>`_ (pour s'abonner, voir `ici
  <https://framalistes.org/sympa/info/markas>`_)

Sur FramaGit
============

Il est possible de `créer un ticket
<https://framagit.org/markas/infrastructure/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=>`_
sur notre forge pour poser une question, remonter un problème,
demander une amélioration.
