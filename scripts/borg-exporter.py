# This exporter generates a prom file from the borg info output
#
# Usage: borg info REPOSTIORY --json --last 1 | borg-exporter.py PUSH-GATEWAY-ADDRESS:PORT  # noqa

import sys
import json
import urllib.error
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway

data = json.load(sys.stdin)

registry = CollectorRegistry()
g = Gauge('borg_last_success_unixtime',
          'Last time a batch job successfully finished',
          registry=registry)
g.set_to_current_time()

g = Gauge('borg_nfiles',
          'Number of archived files',
          registry=registry)
g.set(data['archives'][0]['stats']['nfiles'])

g = Gauge('borg_compressed_size',
          'Compressed size',
          registry=registry)
g.set(data['archives'][0]['stats']['compressed_size'])

g = Gauge('borg_deduplicated_size',
          'Deduplicated size',
          registry=registry)
g.set(data['archives'][0]['stats']['deduplicated_size'])

g = Gauge('borg_original_size',
          'Original size',
          registry=registry)
g.set(data['archives'][0]['stats']['original_size'])

# TODO: add the duration
# print(data['archives'][0]['duration'])

try:
    push_to_gateway(sys.argv[1], job="borg", registry=registry)
except urllib.error.URLError as e:
    print("Connection refused to pushgateway:", e)
