{ pkgs, configuration, sources }:


let

  mlib = import ./lib.nix { inherit pkgs; };
  jq = "${pkgs.jq}/bin/jq";
  password = "recipient";

in pkgs.nixosTest {
  name = "test-prometheus";
  nodes = {
    machine = {pkgs, lib, config, ...}: {
      imports = [
        configuration
        ./common.nix
      ];

      config = {
        virtualisation = {
          diskSize = 1 * 1024;
          memorySize = 1 * 1024;
        };

        # This service is used to validate a prometheus alert is fired
        # when a systemd service failed.
        systemd.services.unitFailed = {
          wantedBy = [ "multi-user.target" ];
          script = "false";
        };

        mailserver = {
          enable = lib.mkForce true;
          loginAccounts = {
            "recipient@markas.fr" = {
              hashedPasswordFile = mlib.hashPassword password;
            };
          };
          enableImapSsl = true;
          localDnsResolver = false;
        };

        services.prometheus = {
          enable = lib.mkForce true;

          alertmanager.configuration = {
            receivers = [
              {
                name = "mail-test";
                email_configs = [
                  {
                    to = "recipient@markas.fr";
                    tls_config.insecure_skip_verify = true;
                  }
                ];
              }
            ];
            route.receiver = "mail-test";
          };

          # To output logs to check alertmanager is trying to send a mail
          alertmanager.logLevel = "debug";
        };

      };
    };
  };

  testScript = ''
    rabi.wait_for_unit("prometheus.service")
    # Prometheus takes a bit of time to expose these job values
    rabi.wait_until_succeeds(
        'curl -L localhost:9090/api/v1/label/job/values | \
        ${jq} -e \'.data | sort | . == ["nextcloud", "node", "prometheus", "pushgateway"]\'''
    )

    rabi.wait_until_succeeds(
        'curl -s -L localhost:9090/api/v1/alerts | \
        ${jq} -e \'.data.alerts[] | select(.labels.alertname=="ServiceFailed") | .state == "firing"\'''
    )

    # Check if the alert manager gets the alert.
    # Note the alertmanager takes around 30sec to be notified.
    rabi.wait_until_succeeds(
        "curl localhost:9093/api/v2/alerts | \
        ${jq} -e '.[] | select(.labels.name == \"unitFailed.service\")'"
    )

    rabi.wait_until_succeeds(
        "${mlib.mailCheck} read --ignore-dkim-spf --imap-username recipient@markas.fr --imap-password ${password} FIRING"
    )
  '';
}
