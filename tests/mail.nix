{ pkgs, configuration }:


let

  mlib = import ./lib.nix { inherit pkgs; };

  password = "recipient";

in

pkgs.nixosTest {
  name = "test-mail";
  nodes = {
    rabi = {pkgs, lib, config, ...}: {

      imports = [
        configuration
        ./common.nix
      ];

      config = {

        virtualisation = {
          diskSize = 1 * 1024;
          memorySize = 1 * 1024;
        };

        mailserver = {
          enable = lib.mkForce true;
          loginAccounts = {
            "recipient@markas.fr" = {
              hashedPasswordFile = mlib.hashPassword password;
            };
          };
          enableImap = true;
          enableImapSsl = true;
          check = {
            enable = lib.mkForce true;
            imapHost = lib.mkForce "localhost";
            dstAddr = lib.mkForce "recipient@markas.fr";
            # Because DKIM and SPF are not available locally
            ignoreDkimSpf = true;
            dstPasswordFile = lib.mkForce (toString (pkgs.writeTextFile {
              name = "check.dstPassowrdFile";
              text = password;
            }));
          };
        };

      };
    };
  };

  testScript = ''
    rabi.wait_for_unit("multi-user.target")
    rabi.wait_until_succeeds("journalctl -u check-mail.service | grep -q 'has been found'")
  '';
}
