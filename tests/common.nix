{ pkgs, lib, ...}:

# Module to disable most services by default
# They still can be enable in tests explicitely with mkForce
# depending what the test is testing :o

# 1000 = mkDefault
# 100 = normal value set
# 50 = mkForce
{

  mailserver.enable = lib.mkOverride 70 false;

  mailserver.check.enable = lib.mkOverride 70 false;

  nextcloud.enable = lib.mkOverride 70 false;

  services.autoUpgrade.enable = lib.mkOverride 70 false;

  services.grafana.enable = lib.mkOverride 70 false;

  services.prometheus.enable = lib.mkOverride 70 false;

  services.backup.enable = lib.mkOverride 70 false;

}
