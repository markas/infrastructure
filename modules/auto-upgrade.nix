# This module checks out the master branch of the repository
# containing the configuration of the machine every minute, build
# this configuration and switch to it.

{ pkgs, config, ... }:
with pkgs.lib;

let
  cfg = config.services.autoUpgrade;

  # We generate a gpg keyrinbg containing the public keys allowed to
  # apply the configuration.
  gpgKeyring = pkgs.runCommand "gpg-keyring" { buildInputs = [ pkgs.git pkgs.gnupg ]; } ''
    # GPG configuation
    export HOME=$PWD
    touch $out
    gpg --no-default-keyring --keyring $out --import ${concatStringsSep " " cfg.keys}
  '';

  gpg = pkgs.writeScript "gpg" "${pkgs.gnupg}/bin/gpg --no-default-keyring --keyring ${gpgKeyring} $@";

in
{
  options = {
    services.autoUpgrade = {
      enable = mkEnableOption "auto-upgrade";

      remotes = mkOption {
        type = with types; listOf str;
        description = ''
          A list of Git repositories to fetch the configuration from.
          The first repository of the list is the first remote used.
        '';
      };
      keys = mkOption {
        type = types.listOf types.path;
        description = ''
          A list of gpg key paths to trust.
        '';
      };
    };
  };
  config = {
    systemd.services.auto-upgrade = mkIf cfg.enable {
      description = "Auto Upgrade";
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = false;
      unitConfig.X-StopOnRemoval = false;
      # Do it every minute
      startAt = "*-*-* *:*:00";
      serviceConfig.TimeoutSec = "3h";
      path = [ pkgs.git pkgs.gnutar pkgs.gzip pkgs.xz.bin config.nix.package.out ];
      script = ''
        set -euo pipefail

        # TODO: use workingDir instead
        mkdir -p /var/nixos/
        cd /var/nixos/
        git init -q

        CURRENT_CID=$(git rev-parse --quiet HEAD 2>/dev/null || true)

        fetched=1
        for i in ${pkgs.lib.concatStringsSep " " cfg.remotes};
        do
          echo "Pulling $i"
          if git pull --quiet $i master;
          then
            fetched=0
            break
          fi
        done

        if [ $fetched -eq 1 ]
        then
          echo No remote could be fetched
          exit 1
        fi

        if [[ $CURRENT_CID != "HEAD" && $CURRENT_CID == $(git rev-parse HEAD) ]];
        then
          exit 0;
        else
          echo New commits:
          git log $CURRENT_CID..HEAD --oneline --no-decorate
        fi

        # We replace the gpg program by the one containing our specific keyring
        git config gpg.program ${gpg}

        if ! git verify-commit HEAD
        then
          echo Could not verify the signature of commit $(git rev-parse HEAD)
          exit 1;
        fi

        echo "Running Nix build..."
        nix-build -A system
        echo "Switching system..."
        # TODO: do not immediately switch to configuration since it could break the machine
        ./result/bin/switch-to-configuration switch
        echo "System have been switched!"
      '';
    };
  };
}
