{ pkgs, config, ...}:

with pkgs.lib;
let
  cfg = config.services.backup;
  borgSSH =  "ssh -oStrictHostKeyChecking=no -i ${cfg.sshPrivateKeyPath}";
  borg = pkgs.writers.writeBashBin "borg" ''
    BORG_PASSPHRASE="$(cat ${cfg.passphrasePath})" BORG_RSH="${borgSSH}" ${pkgs.borgbackup}/bin/borg $@
  '';

in
{
  options = {
    services.backup = {
      enable = mkEnableOption "backup";
      paths = mkOption {
        type = with types; listOf str;
      };
      passphrasePath = mkOption {
        type = types.str;
      };
      sshPrivateKeyPath = mkOption {
        type = types.str;
      };
      repo = mkOption {
        type = types.str;
      };
      startAt = mkOption {
        type = types.listOf types.str;
        default = [];
      };
      # WARNING: this only has to be used in testing environment since
      # it exposes the passphrase!
      borgCommand = mkOption {
        type = types.package;
        default = borg;
        readOnly = true;
        description = "The borg command configured via environment variable. WARNING: this only has to be used in testing environment since it exposes the passphrase!";
      };
    };
  };
  config = {
    keys.borgbackupPasshrase.path = cfg.passphrasePath;
    keys.borgbackupSsshPrivateKey.path = cfg.sshPrivateKeyPath;
    services.borgbackup.jobs = {
      remote = {
        paths = cfg.paths;
        repo = cfg.repo;
        # The repokey type stores the key in the repository itself
        # (but not the passphrase).
        encryption.mode = "repokey-blake2";
        encryption.passCommand = "cat ${cfg.passphrasePath}";
        startAt = cfg.startAt;
        environment.BORG_RSH = borgSSH;
        prune.keep = {
          # Keep all archives from the last month. Older archives are
          # then removed.
          within = "1m";
        };
        postHook = ''
          borg info ${cfg.repo} --last 1
          borg info ${cfg.repo} --json --last 1 | ${pkgs.borg-exporter}/bin/borg-exporter http://localhost:9091
        '';
      };
    };
  };
}
