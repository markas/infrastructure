Infrastructure collaborative markas.fr
=======================================

Ce dépôt contient tout le code de l'infrastructure numérique de
`l'association Markas <https://markas.fr/association.html>`_.


Utiliser les services de Markas
-------------------------------

Les services proposés sont détaillés dans le `guide d'utilisation de
Markas <https://documentation.markas.fr/utiliser.html>`_.


Contribuer à l'infrastructure
-----------------------------

- Pour démarrer le serveur localement

  ::

     ./start-infrastructure

  Ce script lance une machine virtuelle, puis une instance
  Firefox. Cette instance Firefox permet d'accèder aux services web
  hebergés par la machine virtuelle (via un proxy SOCKS)

- Discuter avec nous sur `Matrix ou IRC <https://markas.fr/contact.html#par-messagerie-instantanee>`_
- La section `contribuer <https://documentation.markas.fr/contribuer.html>`_ de la documentation


En savoir plus
--------------

- `Documentation <https://documentation.markas.fr>`_
- `Guide d'utilisation <https://documentation.markas.fr/utiliser.html>`_
- `Description de l'infrastructure <https://documentation.markas.fr/description.html>`_
- `Contact <https://documentation.markas.fr/contact.html>`_


Licence
-------

Le dépôt https://framagit.org/markas/infrastructure est sous license
`AGPLv3
<https://fr.wikipedia.org/wiki/GNU_Affero_General_Public_License>`_

