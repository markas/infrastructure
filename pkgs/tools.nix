self: super:
let
  virtualHosts = builtins.attrNames self.markas-configuration.services.nginx.virtualHosts;
  # This creates the file required by Firefox to trust our CA
  cert = self.runCommand "cert9.db" { buildInputs = [self.nssTools]; } ''
     mkdir $out
     certutil -A -n "selfsigned" -t "TCu,Cuw,Tuw" \
       -i ${self.markas-vm-configuration.testing.ca}/cert.pem \
       -d sql:$out
   '';
in

{
  markas-start-infrastructure = self.writers.writeBash "start-infrastructure" ''
    export NIX_DISK_IMAGE=$(readlink -f ''${NIX_DISK_IMAGE:-./nixos.qcow2})
    rm -f $NIX_DISK_IMAGE
    ${self.foreman}/bin/foreman start --procfile ${self.markas-procfile}
  '';

  markas-procfile = self.writeText "procfile" ''
    firefox: ${self.wait-for}/bin/wait-for -t 3600 localhost:1337 -- ${self.markas-firefox}
    vm: rm -f ./nixos.qcow2 &&  ${self.markas-vm}/bin/run-rabi-vm
    ssh: ${self.wait-for}/bin/wait-for -t 3600 localhost:2222 -- ${self.markas-ssh-socks}
  '';

  markas-ssh-socks = self.writers.writeBash "ssh-socks" ''
    # The SSH port can be listening while the SSH server is not ready yet.
    # We then retry to SSH
    for i in $(seq 1 30); do
      ${self.openssh}/bin/ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -D 1337 -C -N root@localhost -p 2222
      sleep 1
    done
  '';

  markas-firefox = self.writers.writeBash "markas-firefox" ''
    export TMP_DIR=$(mktemp -d)
    trap "{ rm -rf $TMP_DIR; }" EXIT

    echo "Using firefox profile $TMP_DIR/firefox-profile"
    mkdir -p $TMP_DIR/firefox-profile
    ln -s ${../data/firefox-socks/prefs.js} $TMP_DIR/firefox-profile/prefs.js

    cp ${cert}/* $TMP_DIR/firefox-profile/

    ${self.firefox-unwrapped}/bin/firefox --no-remote \
        --new-instance \
        --profile $TMP_DIR/firefox-profile/ \
        ${self.lib.concatStringsSep " " virtualHosts}
  '';

  wait-for = self.stdenv.mkDerivation {
    name = "wait-for";
    src = self.fetchFromGitHub {
      owner = "mrako";
      repo = "wait-for";
      rev = "d9699cb9fe8a4622f05c4ee32adf2fd93239d005";
      sha256 = "10fvdivpm8hr9ywaqyiv56nxrjbqkszmp6rj8zj3530gwdl8yddd";
    };
    buildInputs = [ self.makeWrapper ];
    dontBuild = true;
    installPhase = ''
      mkdir -p $out/bin
      cp wait-for $out/bin/
      chmod 755 $out/bin/wait-for
    '';
    postFixup = "wrapProgram $out/bin/wait-for --argv0 wait-for --set PATH ${self.netcat}/bin:${self.coreutils}/bin";
  };

  borg-exporter =
    self.writers.writePython3Bin
      "borg-exporter" {
        libraries = [ self.python3Packages.prometheus_client ]; }
      (builtins.readFile ../scripts/borg-exporter.py);
}
