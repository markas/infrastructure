{ config, pkgs, sources, ... }:

let
  cfg = config;
in {
  imports = [
    ./hardware-configuration.nix
    ../modules/backup.nix
    ../modules/prometheus.nix
    ../modules/auto-upgrade.nix
    ../modules/vpn.nix
    ../modules/nextcloud.nix
    ../modules/documentation.nix
    ../modules/mail.nix
    ./prometheus.nix
    ./grafana.nix
  ];

  time.timeZone = "Europe/Paris";

  services.journald.enableHttpGateway = true;

  networking = {
    hostName = "rabi";
    domain = "markas.fr";
  };

  services.vpn.member = {
    privateKeyFile = "/var/keys/wireguard-member";
    clients = [
      {
        # lewo's vpn
        ip = "10.100.0.2";
        publicKey = "LL06/OugJg7Wht8fA3dnCoTpLAPfg7/QZqqwdueNT10=";
      }
      {
        # eon's vpn
        ip = "10.100.0.3";
        publicKey = "LLY6gi980BjiO17koQG+1ymYeKcjwW2drH+5JJ2UsUI=";
      }
    ];
  };

  security.acme.defaults.email = "admin@markas.fr";
  security.acme.acceptTerms = true;

  # The port 80 is required by acme
  networking.firewall.allowedTCPPorts = [ 443 80 ];

  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
    };
  };

  services.autoUpgrade = {
    enable = true;
    remotes = [
      "https://framagit.org/markas/infrastructure.git"
      # We can fallback to GitHub if framagit is not responding anymore
      "https://github.com/markas-fr/infrastructure.git"
      # And to a local folder if GitHub is not working:/
      "/tmp/markas-infrastructure"
    ];
    keys = [
      ../keys/lewo.asc
      ../keys/eon.asc
      ../keys/s-degoul.asc
    ];
  };

  environment.systemPackages = with pkgs; [
    smartmontools tmux htop emacs-nox
  ];

  mailserver = {
    enable = true;
    fqdn = "mail.markas.fr";
    domains = [ "markas.fr" ];
    check = {
      enable = true;
      imapHost = "imap.gmail.com";
      dstAddr = "markas.monitoring@gmail.com";
      dstPasswordFile = "/var/keys/mailserver-dst-password";
    };
    forwards = {
      "admin@markas.fr" = [
        "lewo@abesis.fr"
        "eon@patapon.info"
        "samuel.degoul@posteo.net"
      ];
    };
  };

  nextcloud = {
    enable = true;
    adminEmail = "admin@markas.fr";
    apps = with sources; {
      calendar = nextcloud-calendar;
      contacts = nextcloud-contacts;
      notes = nextcloud-notes;
      registration = nextcloud-registration;
      richdocuments = nextcloud-richdocuments;
      groupfolders = nextcloud-groupfolders;
      twofactor_admin = nextcloud-twofactor_admin;
      twofactor_totp = nextcloud-twofactor_totp;
      twofactor_webauthn = nextcloud-twofactor_webauthn;
      twofactor_nextcloud_notification = nextcloud-twofactor_nextcloud_notification;
    };
    phpSettings = {
      default_phone_region = "FR";
      log_type = "syslog";
      maintenance_window_start = 1;
    };
    settings = {
      system = {
        appstoreenabled = false;
        mail_smtphost = "127.0.0.1:25";
        mail_domain = "markas.fr";
        mail_smtpstreamoptions.ssl = {
          allow_self_signed = true;
          verify_peer = false;
          verify_peer_name = false;
        };
      };
      apps.core.backgroundjobs_mode = "cron";
      # This avoids users to see the email of all others users when
      # they try to share a file.
      apps.core.shareapi_only_share_with_group_members = "yes";
      apps.registration.admin_approval_required = "yes";
      # Default quota for new users
      apps.files.default_quota = "40GB";
      apps.richdocuments.wopi_url = "https://office.markas.fr";
      # This is to disable the rich workspace feature because of this issue:
      # https://help.nextcloud.com/t/loading-spinner-in-files-overview/80393
      apps.text.workspace_available = "0";
    };
  };

  services.mysqlBackup = {
    enable = true;
    databases = [ "nextcloud" ];
    singleTransaction = true;
    calendar = "03:15:00";
  };

  services.backup = {
    enable = true;
    # We run the job every day at 4am
    startAt = [ "*-*-* 04:00:00" ];
    repo = "borg@burla.abesis.fr:.";
    passphrasePath = "/var/keys/borgbackup-passphrase";
    sshPrivateKeyPath = "/var/keys/borgbackup-private-key";
    paths = [
      cfg.services.mysqlBackup.location
      cfg.services.nextcloud.home
    ];
  };

  # This is to allow user passwordless: user needing a root account
  # can ssh with root account
  users.extraUsers.root.openssh.authorizedKeys.keyFiles = [
    ../keys/lewo.pub
    ../keys/eon-tron.pub
    ../keys/eon-patron.pub
    ../keys/s-degoul.pub
  ];

  users.extraUsers = {
    lewo = {
      isNormalUser = true;
      description = "Antoine Eiche";
      openssh.authorizedKeys.keyFiles = [ ../keys/lewo.pub ];
    };
    eon = {
      isNormalUser = true;
      description = "Jean-Philippe Braun";
      openssh.authorizedKeys.keyFiles = [ ../keys/eon-patron.pub ../keys/eon-tron.pub ];
    };
    s-degoul = {
      isNormalUser = true;
      description = "Samuel Degoul";
      openssh.authorizedKeys.keyFiles = [ ../keys/s-degoul.pub ];
    };
  };

  system.stateVersion = "22.11";

}
