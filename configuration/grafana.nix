{ pkgs, config, ...}:

let 
  provisionning = pkgs.runCommand "grafana-provisioning" {} ''
    mkdir -p $out/{datasources,dashboards}

    cat <<EOF > $out/datasources/prometheus.yml
    apiVersion: 1
    datasources:
    - name: Prometheus
      type: prometheus
      access: proxy
      orgId: 1
      url: http://localhost:9090
      isDefault: true
      version: 1
      editable: false
    EOF

    cat <<EOF > $out/dashboards/dashboards.yml
    apiVersion: 1
    providers:
    - name: default
      orgId: 1
      folder: ""
      type: file
      disableDeletion: false
      updateIntervalSeconds: 0
      options:
        path: ${../data/grafana}
    EOF
  '';
in
{
  services.nginx.virtualHosts = {
    "grafana.markas.fr" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:3000";
        recommendedProxySettings = true;
      };
    };
  };
  services.grafana = {
    enable = true;
    settings = {
      server.http_addr = "0.0.0.0";
      analytics.reporting_enabled = false;
      # It would be better to remove auth but I don't know how to create dashboard then.
      # We need to comment the line below to be able to create dashboards in the test VM
      "auth.anonymous".enable = true;
      paths.provisioning = provisionning;
    };
  };
}
